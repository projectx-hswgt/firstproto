// ============================================================================
// SETUP DEPENDENCIES =========================================================
// ============================================================================
express = require('express');
cookieParser = require('cookie-parser');
bodyParser = require('body-parser');
app = express();
http = require('http');
path = require('path');

// ============================================================================
// EXPRESS CONFIGURATION ======================================================
// ============================================================================
app.set('port', process.env.PORT || 3000);
app.use(cookieParser());
app.use(bodyParser());
app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');
app.use(express.static(path.join(__dirname, 'public')));

// ============================================================================
// ROUTES =====================================================================
// ============================================================================
require('./app/routes/')(app);

// ============================================================================
// LAUNCH =====================================================================
// ============================================================================
http.createServer(app).listen(app.get('port'), function() {
  console.log('Express server listening on port ' + app.get('port'))
});