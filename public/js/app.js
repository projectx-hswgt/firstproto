var appModule = angular.module('AppModule', ['ngRoute']);

// ============================================================================
// ROUTES =====================================================================
// ============================================================================
appModule.config(function($routeProvider) {
    $routeProvider
      .when('/LogIn', {
        templateUrl: 'fragments/LogIn.html',
        controller: 'AppController'
      })
      //immer diese 4 Zeilen kopieren für das erstellen einer neuen Page (neue Route)
      .when('/projects', {
        templateUrl: 'fragments/projects.html',
        controller: 'AppController'
      })
      .when('/createProject/general', {
        templateUrl: 'fragments/createProject/general.html',
        controller: 'AppController'
      })
      .when('/createProject/challenge', {
        templateUrl: 'fragments/createProject/challenge.html',
        controller: 'AppController'
      })
      .when('/createProject/finance', {
        templateUrl: 'fragments/createProject/finance.html',
        controller: 'AppController'
      })
      .when('/createProject/scope', {
        templateUrl: 'fragments/createProject/scope.html',
        controller: 'AppController'
      })
      .when('/createProject/attachement', {
        templateUrl: 'fragments/createProject/attachement.html',
        controller: 'AppController'
      })
      .otherwise({
        redirectTo: '/LogIn'
      })
  });

// ============================================================================
// VIEW 1 CONTROLLER ==========================================================
// ============================================================================
appModule.controller('AppController', function($scope) {
	$scope.name = 'Login';
});